/* tslint:disable:radix */
import { NestFactory } from '@nestjs/core';
import { AppModule } from './modules/app.module';
import { ZeebeServer } from '@payk/nestjs-zeebe';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const microservice = app.connectMicroservice({
    strategy: app.get(ZeebeServer),
  });

  await app.startAllMicroservicesAsync();
}
bootstrap();
