// Note:
// file
// ...\node_modules\@nestjs\microservices\context\rpc-context-creator.js
// should be replaced with appropriate file from previous version provided in root directory.

import { Controller, Inject } from '@nestjs/common';
import { ZeebeWorker } from '@payk/nestjs-zeebe';
import { RabbitmqPublisherService } from '../services/rabbitmq-publisher.service';
import { SqlFetchService } from '../services/sql-fetch.service';

@Controller()
export class AppController {
  constructor(private readonly rabbitmqPublisherService: RabbitmqPublisherService, // publisher to submit result to gateway
              private readonly service: SqlFetchService) { }

  sendResultToGateway = async (serviceInstanceId: string, sessionId: string, result: any) =>
    await this.rabbitmqPublisherService.publish(serviceInstanceId, { sessionId, result });

  @ZeebeWorker('task-1-1', { maxJobsToActivate: 10, timeout: 300 })
  task1(job, complete) {
    console.log('task-1 -> Task variables', job.variables);

    // Task worker business logic
    const result = '1';

    const variableUpdate = {
      tracer: 'task-1',
      status: 'ok',
      result,
    };

    complete.success(variableUpdate);
  }

  @ZeebeWorker('task-1-2', { maxJobsToActivate: 10, timeout: 300 })
  async task2(job, complete) {
    console.log('task-2 -> Task variables', job.variables);

    // Task worker business logic
    const result = job.variables.result + '2';

    const fetchResult = await this.service.allPersons();

    const variableUpdate = {
      tracer: 'task-2',
      status: 'ok',
      result,
      fetchResult,
      nextTask: 3,
    };

    complete.success(variableUpdate);
  }

  @ZeebeWorker('task-1-3')
  async task13(job, complete) {
    console.log('task-3 -> Task variables', job.variables);

    // Task worker business logic
    const result = `workflow: <b><i>${job.variables.serviceInstanceId}</i></b>  ${job.variables.result}.3... ${job.variables.sessionId}<br/><br/>` +
                   `${JSON.stringify(job.variables.fetchResult)}`;

    const variableUpdate = {
      tracer: 'task-3',
      status: 'ok',
      result,
      fetchResult: undefined
    };

    complete.success(variableUpdate);
    await this.sendResultToGateway(job.variables.serviceInstanceId, job.variables.sessionId, result);
  }

  @ZeebeWorker('task-1-4')
  async task14(job, complete) {
    console.log('task-4 -> Task variables', job.variables);

    // Task worker business logic
    const result = `workflow: <b><i>${job.variables.serviceInstanceId}</i></b>  ${job.variables.result}.4... ${job.variables.sessionId}`;

    const variableUpdate = {
      tracer: 'task-4',
      status: 'ok',
      result,
    };

    complete.success(variableUpdate);
    await this.sendResultToGateway(job.variables.serviceInstanceId, job.variables.sessionId, result);
  }
}
