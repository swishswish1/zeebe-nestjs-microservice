// app.module.ts
import { Module, Inject } from '@nestjs/common';
import { AppController } from '../controllers/app.controller';
import { ZEEBE_CONNECTION_PROVIDER, ZeebeModule, ZeebeServer } from '@payk/nestjs-zeebe';
import { ZBClient } from 'zeebe-node';
import { RabbitmqPublisherService } from '../services/rabbitmq-publisher.service';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SqlFetchService } from '../services/sql-fetch.service';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot(),
    ZeebeModule.forRoot({
      gatewayAddress: 'localhost',
      options: { loglevel: 'INFO', longPoll: 30000 }
    }),
  ],
  controllers: [AppController],
  providers: [SqlFetchService, ZeebeServer, RabbitmqPublisherService]
})
export class AppModule {
  constructor(@Inject(ZEEBE_CONNECTION_PROVIDER) private readonly zbClient: ZBClient) {
    this.zbClient.deployWorkflow(`./bpmn/${process.env.ZEEBE_WORKFLOW_1}.bpmn`).then(res => {
      console.log('Workflow deployed:');
      console.log(res);
    });
  }
}
