import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import { Person } from '../entities/person.entity';

@Injectable()
export class SqlFetchService {
  constructor(private connection: Connection) { }

  allPersons = async (): Promise<Person[]> =>
    await this.connection.getRepository(Person).find();
}
