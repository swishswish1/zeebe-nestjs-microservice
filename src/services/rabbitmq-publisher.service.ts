import { Injectable } from '@nestjs/common';
import { Publisher } from 'rabbitmq-provider/publisher';

@Injectable()
export class RabbitmqPublisherService {
  publishers = new Map<string, Publisher>();

  private static createPublisher = async (queue: string) =>
    await Publisher.createPublisher({
        connUrl: process.env.RABBITMQ_CONNECTION_URL,
        queue,
        durable: true,
        persistent: true,
      },
      null);

  async publish(serviceInstanceId: string, msg: any) {
    let publisher = this.publishers.get(serviceInstanceId);
    if (publisher === undefined) {
      publisher = await RabbitmqPublisherService.createPublisher(serviceInstanceId);
      this.publishers.set(serviceInstanceId, publisher);
    }

    publisher.publish(msg);
  }
}
